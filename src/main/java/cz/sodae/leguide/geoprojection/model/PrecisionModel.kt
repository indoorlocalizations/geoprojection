package cz.sodae.leguide.geoprojection.model

import org.locationtech.jts.geom.PrecisionModel

/**
 * Geometry model
 */
fun providePrecisionModel(): PrecisionModel {
    return PrecisionModel(PrecisionModel.FLOATING)
}