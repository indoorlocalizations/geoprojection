package cz.sodae.leguide.geoprojection.model.signal

import cz.sodae.leguide.geoprojection.elements.BleBeacon
import cz.sodae.leguide.geoprojection.utils.angleDistance
import cz.sodae.leguide.geoprojection.utils.convertToCircleAngle
import kotlin.math.pow

/**
 * Sample
 * @param deviceOrientation is in circle angle (not compass angle)
 */
data class Sample(
        val beacon: BleBeacon,
        val rssi: Double,
        val deviceOrientation: Double?
)

object SignalDistance {

    /**
     * Converts one value to meters
     */
    fun one(sample: Sample): Double = when {
        sample.deviceOrientation == null -> distanceFront(sample.rssi)
        else -> when (angleDistance(convertToCircleAngle(sample.beacon.orientation), sample.deviceOrientation)) {
            in -90..90 -> distanceBack(sample.rssi)
            else -> distanceFront(sample.rssi)
        }
    }

    /** Distance: device is front on transmitter **/
    fun distanceFront(rssi: Double): Double {
        return 1.747292e-14 * (-rssi).pow(7.765577) // new
        //return 6.152468e-10 * (-rssi).pow(5.252333)
    }

    /**
     * Distance: device is back on transmitter
     * @note it uses Front equation because it returns wrong results
     **/
    fun distanceBack(rssi: Double): Double {
        return distanceFront(rssi)
        // return 9.594377e-19 * (-rssi).pow(9.850514)
    }

}
