package cz.sodae.leguide.geoprojection.model.signal.filters

import cz.sodae.leguide.geoprojection.process.BeaconsDistances
import cz.sodae.leguide.geoprojection.process.Signal
import io.reactivex.Flowable

interface FilterBuilder {

    /**
     * @return Flowable object which produces distances
     */
    fun build(): Flowable<BeaconsDistances>

}
