package cz.sodae.leguide.geoprojection.model.signal.filters

import cz.sodae.leguide.geoprojection.model.signal.Sample
import cz.sodae.leguide.geoprojection.process.BeaconsDistances
import io.reactivex.Flowable

/**
 * @param maxLife Maximum life time in milliseconds of Kalman filter without signal
 */
class KalmanFilterBuilder(
        val signalSamples: Flowable<Sample>
) : FilterBuilder {

    override fun build(): Flowable<BeaconsDistances> {
        val state = KalmanFilterState()

        return signalSamples.map {
            state.tick(listOf(it))
        }
    }
}
