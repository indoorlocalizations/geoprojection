package cz.sodae.leguide.geoprojection.model

import cz.sodae.leguide.geoprojection.Point
import cz.sodae.leguide.geoprojection.graph2.LevelPoint
import cz.sodae.leguide.geoprojection.graph2.PathFinder
import org.apache.commons.math3.distribution.NormalDistribution
import org.locationtech.jts.geom.Coordinate
import org.locationtech.jts.geom.Geometry
import org.locationtech.jts.geom.GeometryFactory
import org.locationtech.jts.geom.MultiPoint
import org.locationtech.jts.shape.random.RandomPointsBuilder
import org.locationtech.jts.util.GeometricShapeFactory
import kotlin.math.absoluteValue

/**
 * Position is moved like animation walking between two points due to path finding between them
 * @note this is not necessary, but can be fine for users
 * @note it uses just one particle, so there is no particle filter required.
 * @note this is nice to have feature
 */
class ParticleFilterMovementProbability(
        private var floor: Geometry,
        private val pathFinder: PathFinder
) : ParticleFilterProbability {

    private val factory = GeometryFactory(providePrecisionModel())

    // 0. step
    var orientationDegree: Double = -1.0
    var movementDetected: Boolean = false

    val debugPath
        get() = path?.map { it.first } ?: listOf()

    private var lastChangeTs: Long = 0
    private val metersPerSecond = 1.0

    private var path: List<Pair<Point, Double>>? = null // Point, distance between first and this point (over all points)
    private var lastPosition: Point? = null
    private var lastLevel: Double? = null

    private var lastProvidedPosition: Point? = null
    private var lastProvidedLevel: Double? = null

    fun setPosition(position: Point, level: Double) {

        lastProvidedLevel = level
        lastProvidedPosition = position

        val theoreticalPath = pathFinder.findPath(
                LevelPoint(
                        lastPosition ?: position,
                        lastLevel ?: level,
                        null
                ),
                LevelPoint(
                        position,
                        level,
                        null
                )
        )

        if (theoreticalPath.isEmpty()) {
            return
        }

        path = null
        if (theoreticalPath.isNotEmpty()) {
            var cumulative = 0.0
            path = (listOf(theoreticalPath[0]) + theoreticalPath) // we need first point also
                    .filter { it.level == level }
                    .map { it.point } // @todo I think this is buggy maybe
                    .zipWithNext { a, b ->
                        cumulative += a.distance(b)
                        Pair(b, cumulative)
                    }
        }
    }

    fun changeFloor(geometry: Geometry) {
        floor = geometry
    }

    override fun randomPositions(count: Int): List<Point> {
        val builder = RandomPointsBuilder()
        builder.setExtent(floor)
        builder.setNumPoints(count)
        return (builder.geometry as MultiPoint).coordinates.map {
            Point(it.x, it.y, 0.0)
        }
    }

    // 1. step
    override fun predictPosition(point: Point): Point {
        return calculateNewPosition(point)
    }

    override fun probability(point: Point): Double {
        if (lastPosition == null) return 0.0

        return when {
            floor.contains(factory.createPoint(Coordinate(point.x, point.y, point.z))) -> 0.05 // @todo
            else -> 0.0
        }
    }

    override fun recovery(points: List<Point>): List<Point> {
        return lastProvidedPosition?.let {
            val factory = GeometricShapeFactory()
            factory.setSize(10.0)
            factory.setNumPoints(16)
            factory.setCentre(Coordinate(it.x, it.y, 0.0))
            val circle = floor.intersection(factory.createCircle())

            val builder = RandomPointsBuilder()
            builder.setExtent(circle)
            builder.setNumPoints(points.size)
            return (builder.geometry as MultiPoint).coordinates.map {
                Point(it.x, it.y, 0.0)
            }
        } ?: points

    }

    private fun calculateNewPosition(point: Point): Point {
        if (path == null || path!!.size < 2) {
            return point
        }

        val maxDistance = path!!.last().second
        val distance = Math.min(NormalDistribution(
                0.0,
                Math.max(0.6, ((System.currentTimeMillis() - lastChangeTs) / 1000) * metersPerSecond)
        ).sample().absoluteValue, maxDistance)

        val zipped = path!!.zipWithNext()
        val points = zipped.firstOrNull {
            distance in it.first.second..it.second.second
        } ?: zipped.last()
        return points.let {
            val coef = (distance - it.first.second) / (it.second.second - it.first.second)
            Point(
                    (it.first.first.x * (1 - coef) + it.second.first.x * coef),
                    (it.first.first.y * (1 - coef) + it.second.first.y * coef),
                    0.0
            )
        }
    }

    override fun beforeStep(particleFilter: ParticleFilter) {

    }

    override fun afterStep(particleFilter: ParticleFilter) {
        lastPosition = particleFilter.mean
        lastChangeTs = System.currentTimeMillis()
    }

    override fun needRecovery(): Boolean {
        return false
    }
}
