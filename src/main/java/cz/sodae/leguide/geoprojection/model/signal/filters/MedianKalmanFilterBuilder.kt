package cz.sodae.leguide.geoprojection.model.signal.filters

import cz.sodae.leguide.geoprojection.model.signal.Sample
import cz.sodae.leguide.geoprojection.process.BeaconsDistances
import io.reactivex.Flowable
import java.util.concurrent.TimeUnit

/**
 * Kalman filter median filter
 * @param maxLife Maximum life time in milliseconds of Kalman filter without signal
 */
class MedianKalmanFilterBuilder(
        val signalSamples: Flowable<Sample>,
        val samplingTime: Long,
        val maxLife: Long = 3000
) : FilterBuilder {

    override fun build(): Flowable<BeaconsDistances> {
        val state = KalmanFilterState(maxLife)

        return signalSamples
                .buffer(1000, samplingTime, TimeUnit.MILLISECONDS)
                .map {
                    state.tick(
                            it.groupBy {
                                it.beacon
                            }.map {
                                Sample(
                                        it.key,
                                        it.value.map { it.rssi }.average(),
                                        it.value.mapNotNull { it.deviceOrientation }.average()
                                )
                            }
                    )
                }
    }
}
