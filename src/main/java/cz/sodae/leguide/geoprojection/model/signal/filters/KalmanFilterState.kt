package cz.sodae.leguide.geoprojection.model.signal.filters

import cz.sodae.leguide.geoprojection.elements.BleBeacon
import cz.sodae.leguide.geoprojection.model.signal.Sample
import cz.sodae.leguide.geoprojection.model.signal.SignalDistance
import cz.sodae.leguide.geoprojection.process.BeaconsDistances
import org.apache.commons.math3.filter.DefaultMeasurementModel
import org.apache.commons.math3.filter.DefaultProcessModel
import org.apache.commons.math3.filter.KalmanFilter
import org.apache.commons.math3.linear.Array2DRowRealMatrix
import org.apache.commons.math3.linear.ArrayRealVector
import org.apache.commons.math3.linear.RealMatrix

/**
 * Kalman filter state
 * @param maxLife Maximum life time in milliseconds of Kalman filter without signal
 */
class KalmanFilterState(val maxLife: Long = 3000) {

    fun tick(measurement: List<Sample>): BeaconsDistances {
        synchronized(this) {
            measurement.map {
                val distance = SignalDistance.one(it)
                val kf = getKalman(it.beacon, distance)

                kf.kalmanFilter.correct(doubleArrayOf(distance)) // creates if not exists
                kf.lastCorrect = System.currentTimeMillis()
            }

            kalmans.forEach {
                it.value.kalmanFilter.predict()
            }

            kalmans.filter {
                System.currentTimeMillis() - it.value.lastCorrect > maxLife
            }.forEach {
                kalmans.remove(it.key)
            }

            return kalmans.mapValues {
                it.value.kalmanFilter.stateEstimation[0]
            }
        }
    }

    /**
     * Creates or return created Kalman filter for specific beacon by [identifier]
     */
    private fun getKalman(identifier: BleBeacon, defaultDistance: Double): KalmanWithLife {
        return kalmans.getOrPut(identifier) {
            val A = Array2DRowRealMatrix(doubleArrayOf(1.0))
            val B: RealMatrix? = null
            val H = Array2DRowRealMatrix(doubleArrayOf(1.0))
            val Q = Array2DRowRealMatrix(doubleArrayOf(0.001)) // Q - process noise covariance matrix
            val R = Array2DRowRealMatrix(doubleArrayOf(3.0)) // R - measurement noise covariance matrix

            val initState = ArrayRealVector(doubleArrayOf(defaultDistance))
            val initError = null

            val pm = DefaultProcessModel(A, B, Q, initState, initError)
            val mm = DefaultMeasurementModel(H, R)
            KalmanWithLife(
                    KalmanFilter(pm, mm),
                    System.currentTimeMillis()
            )
        }
    }

    /**
     * Kalman filter for each beacon
     */
    private val kalmans = mutableMapOf<BleBeacon, KalmanWithLife>()

    inner class KalmanWithLife(
            val kalmanFilter: KalmanFilter,
            var lastCorrect: Long
    )
}