package cz.sodae.leguide.geoprojection.model.signal.filters

import cz.sodae.leguide.geoprojection.model.signal.Sample
import cz.sodae.leguide.geoprojection.model.signal.SignalDistance
import cz.sodae.leguide.geoprojection.process.BeaconsDistances
import io.reactivex.Flowable
import java.util.concurrent.TimeUnit

/**
 * Average filter, buffers values for last [bufferTime]ms, samples every [samplingTime]ms
 */
class AverageFilterBuilder(
        val signalSamples: Flowable<Sample>,
        val samplingTime: Long,
        val bufferTime: Long = 2000
) : FilterBuilder {

    override fun build(): Flowable<BeaconsDistances> {
        return signalSamples
                .buffer(bufferTime, samplingTime, TimeUnit.MILLISECONDS)
                .map {
                    it
                            .groupBy { it.beacon }
                            .mapValues {
                                it.value.map { SignalDistance.one(it) }.average()
                            }.map {
                                it.key to it.value
                            }.toMap()
                }
    }
}
