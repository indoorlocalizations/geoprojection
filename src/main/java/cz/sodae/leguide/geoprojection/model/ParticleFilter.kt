package cz.sodae.leguide.geoprojection.model

import cz.sodae.leguide.geoprojection.Point
import cz.sodae.leguide.geoprojection.meanPoints
import cz.sodae.leguide.geoprojection.utils.RandomSelector
import kotlin.math.pow
import kotlin.math.sqrt

/**
 * Implementation of Particle filter
 */
class ParticleFilter(
        private val model: ParticleFilterProbability,
        val particlesCount: Int
) {

    var iteration = 0L

    private lateinit var particles: List<Point>

    val particlePositions
        get(): List<Point> {
            return particles
        }

    val mean: Point
        get() = meanPoints(particles)

    val distanceStandardDeviation: Double
        get(): Double {
            val currentMean = mean
            return particles
                    .sumByDouble { it.distance(currentMean).pow(2.0) }
                    .div(particles.size)
                    .let { sqrt(it) }
        }


    fun step() {
        if (iteration == 0L) {
            particles = model.randomPositions(particlesCount)
        }
        iteration++

        model.beforeStep(this)

        // 1 & 2 prediction and probability
        val newParticles = particles
                .map {
                    model.predictPosition(it)
                }
                .map {
                    Particle(it, model.probability(it))
                }

        // 3. weight normalization

        val totalWeight = newParticles.map { it.weight }.sum()

        check(totalWeight != Double.NaN)

        if (totalWeight == 0.0 || model.needRecovery()) {
            particles = model.recovery(particles)
            model.afterStep(this)
            return
        }

        val selector = RandomSelector<Point>()

        newParticles.forEach {
            selector.add(it.point, it.weight / totalWeight)
        }

        // 4. resample
        particles = particles.map {
            selector.pick()
        }

        model.afterStep(this)
    }


    private data class Particle(
            val point: Point,
            var weight: Double = 0.0
    )
}
