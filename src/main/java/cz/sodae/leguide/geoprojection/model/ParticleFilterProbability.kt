package cz.sodae.leguide.geoprojection.model

import cz.sodae.leguide.geoprojection.Point

/**
 * Particle filter probability model
 */
interface ParticleFilterProbability {

    /**
     * init phase
     */
    fun randomPositions(count: Int): List<Point>

    /**
     * 1. step after measurement
     */
    fun predictPosition(point: Point): Point

    /**
     * 2. step weight particles
     */
    fun probability(point: Point): Double

    /**
     * Method is called if probability of all points is zero or [needRecovery] is true
     */
    fun recovery(points: List<Point>): List<Point>

    /**
     * Callback after step
     */
    fun afterStep(particleFilter: ParticleFilter)

    /**
     * Callback before step
     */
    fun beforeStep(particleFilter: ParticleFilter)

    /**
     * Need model recovery?
     */
    fun needRecovery(): Boolean
}
