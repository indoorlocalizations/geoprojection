package cz.sodae.leguide.geoprojection.model

import com.lemmingapex.trilateration.NonLinearLeastSquaresSolver
import com.lemmingapex.trilateration.TrilaterationFunction
import cz.sodae.leguide.geoprojection.Point
import cz.sodae.leguide.geoprojection.building.Building
import cz.sodae.leguide.geoprojection.elements.Room
import cz.sodae.leguide.geoprojection.elements.RoomType
import cz.sodae.leguide.geoprojection.graph.PathFinder
import cz.sodae.leguide.geoprojection.meanPoints
import cz.sodae.leguide.geoprojection.utils.angleDistance
import cz.sodae.leguide.geoprojection.utils.anglePoint2D
import org.apache.commons.math3.distribution.NormalDistribution
import org.apache.commons.math3.fitting.leastsquares.LeastSquaresOptimizer
import org.apache.commons.math3.fitting.leastsquares.LevenbergMarquardtOptimizer
import org.locationtech.jts.geom.Coordinate
import org.locationtech.jts.geom.Geometry
import org.locationtech.jts.geom.GeometryFactory
import org.locationtech.jts.geom.MultiPoint
import org.locationtech.jts.shape.random.RandomPointsBuilder
import org.locationtech.jts.util.GeometricShapeFactory
import kotlin.math.absoluteValue
import kotlin.math.pow
import kotlin.math.roundToInt

data class BeaconMeasurement(val beaconPosition: Point, val distance: Double)

/**
 * Particle filter model for localisation
 */
class ParticleFilterBuildingProbability(private var building: Building) : ParticleFilterProbability {

    /**
     * Geometry factory
     */
    private val factory = GeometryFactory(providePrecisionModel())

    /**
     * Last time of iteration
     */
    private var lastChangeTs: Long = System.currentTimeMillis()

    /**
     * Human speed per second
     */
    private val metersPerSecond = 0.6

    /**
     * Iteration values for location:
     * [orientationDegree] represents user orientation in degree (circle angle, not compass angle)
     * [movementDetected] user is moving
     * [distances] measured (filtered) distances from beacons
     */
    var orientationDegree: Double = 0.0
    var movementDetected: Boolean = false
    var distances: List<BeaconMeasurement> = listOf()

    /**
     * Level information for current localisation
     */
    private var level: Double = building.levels.first()
    private var floor: Geometry = building.getGeometryOnLevel(level)

    /**
     * Information about last iteration
     */
    private var lastMean: Point? = null
    private var lastLevel: Double? = null
    private var lastRoom: Room? = null
    private var lastCountOfDistances = 0

    /**
     * Time between last and current iteration in milliseconds
     */
    private var deltaTime = Double.MIN_VALUE

    /**
     * Stress value for [needRecovery]
     */
    private var stressValue: Double = 0.0

    /**
     * Point from trilateration
     */
    private var optimumPoint: Point? = null

    /**
     * Phase of localisation, it helps don't be sure to PF at first time
     */
    private var initializing = true

    /**
     * Change level
     */
    fun changeFloor(level: Double) {
        floor = building.getGeometryOnLevel(level)
        this.level = level
    }

    /**
     * 0. step, initialize particles in whole floor
     */
    override fun randomPositions(count: Int): List<Point> {
        val builder = RandomPointsBuilder()
        builder.setExtent(floor)
        builder.setNumPoints(count)
        return (builder.geometry as MultiPoint).coordinates.map {
            Point(it.x, it.y, 0.0)
        }
    }

    /**
     * 1. step, predicts next position of particle
     */
    override fun predictPosition(point: Point): Point {
        return when (movementDetected) {
            true -> calculateWalkingPosition(point)
            false -> calculateStandingPosition(point)
        }
    }

    /**
     * 2. step, calculates position
     */
    override fun probability(point: Point): Double {
        // holds particles by type of room
        val roomCoef = when {
            lastRoom?.geometry?.contains(factory.createPoint(Coordinate(point.x, point.y, 0.0))) ?: false -> 1.5
            else -> when (building.roomAt(point, level)?.roomType) {
                RoomType.CORRIDOR -> 1.2
                RoomType.ROOM -> 0.8
                else -> return 0.0 // optimisation, skip another coefficients
            }
        }

        // trilateration optimum point, it helps to find better position
        val trilaterationExtension = optimumPoint?.let {
            val distanceDistribution = NormalDistribution(0.0, 2.0)
            val distance = it.distance(point)
            distanceDistribution.probability(Double.NEGATIVE_INFINITY, -distance)
        } ?: 0.0

        // holds position by distance of beacons
        val probabilityByBeacons = when {
            distances.isEmpty() -> 0.0
            else -> distances // probability distance
                    .map {
                        val distance = (it.distance - point.distance(it.beaconPosition)).absoluteValue

                        val distanceDistribution = NormalDistribution(0.0, it.distance / 3)
                        distanceDistribution.probability(Double.NEGATIVE_INFINITY, -distance)
                    }
                    // total probability
                    .map { it.pow(2) }
                    .reduceRight(Double::plus)
                    .let { Math.sqrt(it) }
        }

        // holds movement with user
        val movementProbability = when {
            !movementDetected -> 0.0000001
            else -> (lastMean?.let {
                val distributionCircle = NormalDistribution(
                        0.0,
                        deltaTime * metersPerSecond / 3
                )
                val distributionAngle = NormalDistribution(
                        0.0,
                        45.0
                )

                val r = it.distance(point)

                val newPointAngle = anglePoint2D(it, point) - 90
                val angleDistance = angleDistance(orientationDegree, newPointAngle)

                val probabilityDistance = 0.0
                distributionCircle.probability(
                        Double.NEGATIVE_INFINITY,
                        -r
                )
                val probabilityAngle = distributionAngle.probability(
                        Double.NEGATIVE_INFINITY,
                        -(angleDistance.absoluteValue)
                )

                Math.sqrt(Math.pow(probabilityDistance, 2.0) + Math.pow(probabilityAngle, 2.0))
            } ?: 0.0)
        }

        val sized = (movementProbability / distances.size.plus(1).toDouble())
        return roomCoef * (trilaterationExtension + probabilityByBeacons + sized)
    }

    /**
     * Recovery particles
     * If stress is above 200, it reset all.
     * Otherwise it resample particles to point within distance (between last position and trilateration position)*1.5
     */
    override fun recovery(points: List<Point>): List<Point> {
        println("recovery")

        if (stressValue > 200) {
            stressValue = 0.0
            return randomPositions(points.size)
        }

        if (distances.isEmpty()) {
            return points
        }
        stressValue = 0.0

        val recoveryPosition = lastMean ?: optimumPoint ?: return randomPositions(points.size)

        val r = (lastMean?.let {
            optimumPoint?.distance(it)
        } ?: 100.0) * 1.5

        val factory = GeometricShapeFactory()
        factory.setSize(r) // r = 7.5m
        factory.setNumPoints(16)
        factory.setCentre(Coordinate(recoveryPosition.x, recoveryPosition.y, 0.0))
        val circle = floor.intersection(factory.createCircle())

        val builder = RandomPointsBuilder()
        builder.setExtent(circle)
        builder.setNumPoints(points.size)
        return (builder.geometry as MultiPoint).coordinates.map {
            Point(it.x, it.y, 0.0)
        }
    }

    /**
     * Predicts position for walking
     */
    private fun calculateWalkingPosition(point: Point): Point {
        val distribution = NormalDistribution(
                0.0,
                metersPerSecond / 3
        )

        return Point(
                point.x + distribution.sample(),
                point.y + distribution.sample(),
                0.0
        )
    }

    /**
     * Predicts position for standing
     */
    private fun calculateStandingPosition(point: Point): Point {
        val distribution = NormalDistribution(
                0.0,
                1.0 / 3
        )
        return Point(
                point.x + distribution.sample(),
                point.y + distribution.sample(),
                0.0
        )
    }

    /**
     * Prepares values for the iteration
     */
    override fun beforeStep(particleFilter: ParticleFilter) {
        deltaTime = Math.max(Double.MIN_VALUE, (System.currentTimeMillis() - lastChangeTs) / 1000.0)
        lastChangeTs = System.currentTimeMillis()

        optimumPoint = if (distances.size > 2) {
            val blePositions = distances
                    .map { doubleArrayOf(it.beaconPosition.x, it.beaconPosition.y) }
                    .toTypedArray()
            val bleDistances = distances
                    .map { it.distance }
                    .toDoubleArray()

            val solver = NonLinearLeastSquaresSolver(TrilaterationFunction(blePositions, bleDistances), LevenbergMarquardtOptimizer())
            val point = solver.solve().point.toArray()
            val p = Point(point[0], point[1], 0.0)
            if (floor.contains(factory.createPoint(Coordinate(p.x, p.y, p.z)))) {
                p
            } else {
                null
            }
        } else {
            null
        }

        stressValue = Math.max(0.0, stressValue + when {
            distances.isEmpty() -> 1
            (optimumPoint?.distance(particleFilter.mean) ?: 1.0) > 3.0 -> 1
            else -> -1
        })

        println(stressValue)
    }

    /**
     * Saves some values after the iteration
     */
    override fun afterStep(particleFilter: ParticleFilter) {
        if (distances.size >= 2) {
            val newMean = particleFilter.mean

            level.let {
                lastRoom = building.roomAt(newMean, it)
                lastLevel = it
            }

            lastMean = newMean

            initializing = false
        }
        lastCountOfDistances = distances.size
    }

    /**
     * Checks if particles need to be reinitialize
     */
    override fun needRecovery(): Boolean {
        if (initializing && distances.size == 2) {
            return true
        }
        return stressValue > 100
    }
}
