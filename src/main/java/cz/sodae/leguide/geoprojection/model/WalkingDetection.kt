package cz.sodae.leguide.geoprojection.model

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics

class WalkingAccelerometer(val x: Double, val y: Double, val z: Double)

/**
 * Level detection
 * @algorithm it detects by standard deviation between 0.3 and 1.3 on x and y axis
 */
class WalkingDetection {

    var last = false
    var beforeLast = false

    /**
     * It saves last two results from previous detection and current detection
     * and if two values is true, then
     * @param values one or two second data
     */
    fun predict(values: List<WalkingAccelerometer>): Boolean {
        val new = detect(values)
        val result = new && last || new && beforeLast || beforeLast && last
        beforeLast = last
        last = new
        return result
    }

    private fun detect(values: List<WalkingAccelerometer>): Boolean {
        val statsX = DescriptiveStatistics()
        val statsY = DescriptiveStatistics()

        values.forEach {
            statsX.addValue(it.y)
            statsY.addValue(it.y)
        }

        val sdX = statsX.standardDeviation
        val sdY = statsY.standardDeviation
        return sdX in 0.3..1.3 && sdY in 0.3..1.3
    }

}
