package cz.sodae.leguide.geoprojection.model

import cz.sodae.leguide.geoprojection.elements.Beacon
import cz.sodae.leguide.geoprojection.elements.BleBeacon

/**
 * Level detection, it just weight average of beacons with level
 */
class LevelDetection(private val existingLevels: List<Double>) {

    var lastLevel: Double = 0.0

    fun detect(samples: List<LevelDetectionSample>): Double {
        check(samples.isNotEmpty())

        lastLevel = samples
                .filter { it.beacon is BleBeacon }
                .flatMap {
                    val level = (it.beacon as BleBeacon).level
                    val points = when {
                        level.exactlyLevel(lastLevel) -> 2.0
                        level.oneLevel() -> 1.5
                        level.contains(lastLevel) -> 1.0
                        else -> 0.5
                    } * (1 / it.beaconDistance)

                    existingLevels.filter { level.contains(it) }.map {
                        Pair(it, points)
                    }
                }
                .groupBy { it.first }
                .mapValues {
                    // each key has sum of points
                    it.value.sumByDouble {
                        it.second
                    }
                }.let {
                    it.maxBy { it.value }!!.key
                }
        return lastLevel

    }

    class LevelDetectionSample(
            val beacon: Beacon,
            val beaconDistance: Double
    )

}
