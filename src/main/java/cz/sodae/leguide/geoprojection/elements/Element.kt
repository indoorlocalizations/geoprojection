package cz.sodae.leguide.geoprojection.elements

import cz.sodae.leguide.geoprojection.toXYPoint
import org.locationtech.jts.geom.Geometry
import org.locationtech.jts.geom.LineString
import org.locationtech.jts.geom.Point

/**
 * Object represents level where is object
 * It supports only one range (e.g. level=1-2) or one level (e.g. level=1)
 */
data class Levels(val lower: Double, val upper: Double) {

    /**
     * It provided level in the levels
     * @example element.level in Levels(1, 2)
     */
    operator fun contains(level: Double): Boolean {
        return level in lower..upper
    }

    /**
     * Does object represent one level only
     */
    fun oneLevel(): Boolean {
        return lower == upper
    }

    /**
     * Does object represent specific one level only
     * @param level specific level
     */
    fun exactlyLevel(level: Double): Boolean {
        return oneLevel() && lower == level
    }

}

/**
 * Describes element featureGeometry and level
 */
open class Element(val level: Levels, val geometry: Geometry) {

    /**
     * Identifier is used to distinct element
     */
    var identifier: String? = null

    /**
     * Name in element
     */
    var name: String? = null

}

/**
 * Abstract class to implements point
 */
open class PointElement(level: Levels, position: Point) : Element(level, position) {

    /**
     * Get Point in JTS format
     */
    val geometryPoint: Point
        get() = geometry as Point

    /**
     * Get Point in this library format
     */
    val point: cz.sodae.leguide.geoprojection.Point
        get() = geometryPoint.coordinate.toXYPoint()
}

/**
 * Type of travel type
 */
enum class LevelTravelType {
    STAIRS,
    ESCALATOR,
    ELEVATOR,
    NONE
}

/**
 * Is room or corridor (or other?)
 */
enum class RoomType {
    ROOM,
    CORRIDOR,
    OTHER
}

/**
 * Door is entrance or other
 */
enum class DoorType {
    ENTRANCE,
    GENERIC
}

/**
 * Toilets by gander
 */
enum class ToiletAmenity {
    MALE,
    FEMALE,
    BOTH,
    NONE
}

/**
 * Element represents room
 */
open class Room(level: Levels, geometry: Geometry) : Element(level, geometry) {
    var levelTravelType = LevelTravelType.NONE
    var roomType = RoomType.OTHER
    var toiletAmenity = ToiletAmenity.NONE
}

/**
 * Element represents door
 */
class Door(level: Levels, geometry: Point) : PointElement(level, geometry) {
    var doorType: DoorType = DoorType.GENERIC
}

/**
 * Element represents elevator
 */
class Elevator(level: Levels, geometry: Point) : PointElement(level, geometry)

/**
 * Element represents footway
 */
class Footway(level: Levels, geometry: LineString) : Element(level, geometry)

/**
 * Element represents steps
 * @param inclineDown does the geometry LineString is built first point is on upper level
 */
class Steps(level: Levels, geometry: LineString, val inclineDown: Boolean = false) : Element(level, geometry) {
    val upperPoint = geometry.let {
        when {
            inclineDown -> geometry.startPoint
            else -> geometry.endPoint
        }.coordinate.toXYPoint()
    }

    val lowerPoint = geometry.let {
        when {
            inclineDown -> geometry.endPoint
            else -> geometry.startPoint
        }.coordinate.toXYPoint()
    }
}

/**
 * Common interface to represents beacons
 */
interface Beacon

/**
 * Ble Beacon element
 * @param mac mac address of beacon
 * @param orientation orientation of beacon in degree like compass
 */
class BleBeacon(
        val mac: String,
        val orientation: Double,
        level: Levels,
        position: Point
) : PointElement(level, position), Beacon
