package cz.sodae.leguide.geoprojection.building

import cz.sodae.leguide.geoprojection.Point
import cz.sodae.leguide.geoprojection.elements.BleBeacon
import cz.sodae.leguide.geoprojection.elements.Element
import cz.sodae.leguide.geoprojection.elements.Room
import cz.sodae.leguide.geoprojection.model.providePrecisionModel
import org.locationtech.jts.geom.Coordinate
import org.locationtech.jts.geom.GeometryFactory
import org.locationtech.jts.util.GeometricShapeFactory

/**
 * Class represents building composed with elements in map
 */
class Building(val elements: List<Element>) {

    /**
     * Geometry factory
     */
    private val factory = GeometryFactory(providePrecisionModel())

    /**
     * United level geometry provider
     */
    private val levelsGeometry = UnionElementGeometry(elements)

    /**
     * Parse levels in file
     */
    val levels = parseLevels()

    /**
     * BLE Beacons
     */
    val beacons = elements.filterIsInstance<BleBeacon>()

    /**
     * BLE beacons by MAC address
     */
    val beaconsByMac = beacons.associateBy { it.mac }

    /**
     * Elements group by level
     */
    val elementsByLevel by lazy {
        elements
                .flatMap { element ->
                    levels
                            .filter {
                                element.level.contains(it)
                            }
                            .map {
                                it to element
                            }
                }
                .groupBy({ it.first }) { it.second }
    }

    /**
     * Provides geometry on level
     */
    fun getGeometryOnLevel(level: Double) = levelsGeometry.getGeometry(level)

    /**
     * Detect room by position (in cartesian map) and level
     */
    fun roomAt(point: Point, level: Double): Room? {
        val geoPoint: org.locationtech.jts.geom.Point = factory.createPoint(
                Coordinate(point.x, point.y)
        )
        return elements
                .filterIsInstance<Room>()
                .filter { it.level.contains(level) }
                .firstOrNull { it.geometry.contains(geoPoint) }
    }

    /**
     * Finds elements around provided point at level within distance in meters by specific type
     */
    inline fun <reified T : Element> nearIs(point: Point, level: Double, distance: Double): List<T> {
        val factory = GeometricShapeFactory()
        factory.setSize(distance * 2)
        factory.setNumPoints(8)
        factory.setCentre(Coordinate(point.x, point.y))
        val circle = factory.createCircle()
        return elements
                .filterIsInstance<T>()
                .filter { it.level.contains(level) }
                .filter { it.geometry.intersects(circle) }
    }

    /**
     * Parse elements
     */
    private fun parseLevels(): List<Double> {
        return elements
                .flatMap {
                    listOf(it.level.lower, it.level.upper)
                }
                .distinct()
                .sorted()
    }

}