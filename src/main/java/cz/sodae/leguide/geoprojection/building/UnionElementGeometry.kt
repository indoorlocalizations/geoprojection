package cz.sodae.leguide.geoprojection.building

import cz.sodae.leguide.geoprojection.elements.Door
import cz.sodae.leguide.geoprojection.elements.Element
import cz.sodae.leguide.geoprojection.elements.Room
import org.locationtech.jts.geom.Geometry
import org.locationtech.jts.util.GeometricShapeFactory

/**
 * Class merge all elements geometry to one geometry
 * Door point radius is one meter size
 */
class UnionElementGeometry(private val elements: Collection<Element>) {

    private var cache = mutableMapOf<Double, Geometry>()

    /**
     * Provides geometry for level
     * Caching enabled - if a geometry have been built it provides the geometry. Otherwise it will build the geometry.
     * @param level
     * @return geometry of level
     */
    fun getGeometry(level: Double): Geometry {
        return cache.getOrPut(level) {
            createFlat(level)
        }
    }

    /**
     * Build geometry by level
     */
    private fun createFlat(level: Double): Geometry {
        return elements
                .filter {
                    level in it.level
                }
                .mapNotNull {
                    when (it) {
                        is Room -> {
                            it.geometry.copy()
                        }
                        is Door -> {
                            val factory = GeometricShapeFactory()
                            factory.setSize(1.0)
                            factory.setNumPoints(8)
                            factory.setCentre(it.geometry.coordinate)
                            factory.createCircle()
                        }
                        else -> null
                    }
                }
                .reduce { acc, obj ->
                    acc.union(obj)
                }
    }

}