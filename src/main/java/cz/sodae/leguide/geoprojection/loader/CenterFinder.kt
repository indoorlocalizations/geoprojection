package cz.sodae.leguide.geoprojection.loader

import com.cube.geojson.*
import cz.sodae.leguide.geoprojection.utils.GeographicTools
import cz.sodae.leguide.geoprojection.utils.WSG84Coordinates
import org.apache.commons.math3.ml.clustering.Clusterable
import org.apache.commons.math3.ml.clustering.KMeansPlusPlusClusterer
import org.apache.commons.math3.ml.distance.DistanceMeasure

private class WSG84CoordinatesClusterable(val coords: WSG84Coordinates) : Clusterable {
    override fun getPoint(): DoubleArray {
        return doubleArrayOf(coords.longitude, coords.latitude)
    }
}

private class WSG84Distance : DistanceMeasure {

    override fun compute(a: DoubleArray?, b: DoubleArray?): Double {
        return GeographicTools.distance(
                WSG84Coordinates(a!![0], a[1]),
                WSG84Coordinates(b!![0], b[1])
        )
    }

}

/**
 * The class find ideal center of GeoJSON element
 */
class FeatureCenterFinder {

    /**
     * @return ideal center of GeoJSON element ([features])
     */
    fun find(features: FeatureCollection): WSG84Coordinates {
        return find(findAllCoordinates(features))
    }

    /**
     * @return ideal center of WSG84Coordinates ([coordinates])
     */
    fun find(coordinates: List<WSG84Coordinates>): WSG84Coordinates {
        if (coordinates.isEmpty()) {
            throw Error("Empty collection to find feature center")
        }

        val kmeans = KMeansPlusPlusClusterer<WSG84CoordinatesClusterable>(10, 50, WSG84Distance())
        val result = kmeans.cluster(coordinates.map {
            WSG84CoordinatesClusterable(it)
        })

        val sum = result.sumBy { it.points.size }
        val centerLong = result.sumByDouble { cluster -> cluster.center.point[0] * cluster.points.size } / sum
        val centerLat = result.sumByDouble { cluster -> cluster.center.point[1] * cluster.points.size } / sum

        return WSG84Coordinates(centerLong, centerLat)
    }

    /**
     * Extracts coordinates from GeoJSON geometry
     */
    private fun findAllCoordinates(features: FeatureCollection): List<WSG84Coordinates> {
        return features.features.flatMap {
            val geometry = it.geometry;
            when (geometry) {
                is Point -> listOf<LngLatAlt>(geometry.coordinates)
                is LineString -> geometry.coordinates
                is Polygon -> geometry.coordinates.flatMap { it }
                else -> listOf()
            }
        }.map { convertCoordinates(it) }
    }

    /**
     * Transform [LngLatAlt] to [WSG84Coordinates]
     */
    private fun convertCoordinates(it: LngLatAlt): WSG84Coordinates {
        return WSG84Coordinates(it.longitude, it.latitude, it.altitude)
    }

}
