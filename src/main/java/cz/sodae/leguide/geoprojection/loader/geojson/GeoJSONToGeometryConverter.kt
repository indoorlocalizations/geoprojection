package cz.sodae.leguide.geoprojection.loader.geojson

import com.cube.geojson.*
import cz.sodae.leguide.geoprojection.model.providePrecisionModel
import org.locationtech.jts.geom.Coordinate
import org.locationtech.jts.geom.Geometry
import org.locationtech.jts.geom.GeometryFactory

class UnsupportedGeometry : Exception()

/**
 * It converts GeoJSON geometry (WSG84) to JTS geometry (cartesian in meters - see [cartesianPositionConverter])
 */
class GeoJSONToGeometryConverter(private val cartesianPositionConverter: CartesianPositionConverter) {

    private val factory = GeometryFactory(providePrecisionModel())

    fun convert(geojsonObject: GeoJsonObject): Geometry {

        return when (geojsonObject) {
            is LineString -> convertLineString(geojsonObject)
            is Point -> convertPoint(geojsonObject)
            is MultiPoint -> convertMultiPoint(geojsonObject)
            is MultiLineString -> convertMultiLineString(geojsonObject)
            is Polygon -> convertPolygon(geojsonObject)
            is MultiPolygon -> convertMultiPolygon(geojsonObject)
            else -> throw UnsupportedGeometry()
        }

    }

    private fun convertPoint(point: Point): Geometry {
        return factory.createPoint(convertCoordinate(point.coordinates))
    }

    private fun convertMultiPoint(multiPoint: MultiPoint): Geometry {
        return factory.createMultiPointFromCoords(convertCoordinate(multiPoint.coordinates))
    }

    private fun convertLineString(lineString: LineString): Geometry {
        return factory.createLineString(convertCoordinate(lineString.coordinates))
    }

    private fun convertMultiLineString(multiLineString: MultiLineString): Geometry {
        return factory.createMultiLineString(multiLineString.coordinates.map {
            factory.createLineString(convertCoordinate(it))
        }.toTypedArray())
    }

    private fun convertPolygon(polygon: Polygon): Geometry {
        if (polygon.coordinates.size > 1) {
            throw UnsupportedGeometry()
        } else {
            return factory.createPolygon(convertCoordinate(polygon.coordinates.first()))
        }
    }

    private fun convertMultiPolygon(multiPolygon: MultiPolygon): Geometry {
        return factory.createMultiPolygon(multiPolygon.coordinates.map {
            factory.createPolygon(convertCoordinate(it.first()))
        }.toTypedArray())
    }

    private fun convertCoordinate(coordinates: Iterable<LngLatAlt>): Array<Coordinate> {
        return coordinates.map {
            convertCoordinate(it)
        }.toTypedArray()
    }

    private fun convertCoordinate(coordinate: LngLatAlt): Coordinate {
        return cartesianPositionConverter.convert(coordinate)
    }

}
