package cz.sodae.leguide.geoprojection.loader.geojson

import com.cube.geojson.Feature
import com.cube.geojson.FeatureCollection
import com.cube.geojson.GeoJsonObject
import com.cube.geojson.Polygon
import cz.sodae.leguide.geoprojection.elements.*
import org.locationtech.jts.geom.Geometry
import org.locationtech.jts.geom.LineString
import org.locationtech.jts.geom.Point

/**
 * [ElementsConverter] converts GeoJSON elements to specific Element (geoprojection)
 * Example: point with properties "door" is Door element
 */
class ElementsConverter(private val geometryConverter: GeoJSONToGeometryConverter) {

    /**
     * Regexp to export levels from regexp
     */
    private val levelRegexp = """^(-?[1-9][0-9]*\.?[0-9]*|[0])(-(-?[1-9][0-9]*\.?[0-9]*)|[0])?$""".toRegex()

    /**
     * Point is beacon with this GeoJSON properties
     * Supported value: "ble" - Bluetooth Low Energy Beacon
     */
    private val beaconKey = "leguide:beacon"

    /**
     * Beacon orientation in compass degree
     */
    private val orientationKeyProperty = "orientation"

    /**
     * BLE beacon mac address
     */
    private val beaconMacKeyProperty = "leguide:beacon_ble_mac"

    /**
     * Converts [FeatureCollection] to [Element] collection
     */
    fun convert(features: FeatureCollection): List<Element> {
        return features.filter { validate(it) }.map {
            absortCommonProperties(when {
                isRoom(it) -> convertRoom(it)
                isDoor(it) -> convertDoor(it)
                isBeacon(it) -> convertBeacon(it)
                isSteps(it) -> convertSteps(it)
                isFootway(it) -> convertFootway(it)
                isElevator(it) -> convertElevator(it)
                else -> convertUnknown(it)
            }, it)
        }
    }

    /**
     * All GeoJSON object must contains level, if there is not - reject the element
     */
    private fun validate(feature: Feature): Boolean {
        return feature.properties.containsKey("level")
    }

    /**
     * Validates the point is beacon
     */
    private fun isBeacon(feature: Feature) = feature.properties.containsKey(beaconKey)

    /**
     * Validates the polygon with property "indoor". The property contains "room" or "corridor"
     */
    private fun isRoom(feature: Feature) = listOf("room", "corridor").contains(feature.properties["indoor"]
            ?: "") && feature.geometry is Polygon

    /**
     * Validates the point with property "door"
     */
    private fun isDoor(feature: Feature) = feature.properties.containsKey("door")

    /**
     * Validates the line string with property "highway". The property contains "steps"
     */
    private fun isSteps(feature: Feature) = "steps" == feature.properties["highway"]

    /**
     * Validates the point with property "highway". The property contains "elevator"
     */
    private fun isElevator(feature: Feature) = "elevator" == feature.properties["highway"]

    /**
     * Validates the line string with property "highway". The property contains "footway"
     */
    private fun isFootway(feature: Feature) = "footway" == feature.properties["highway"]

    /**
     * Converts steps
     */
    private fun convertSteps(feature: Feature): Steps {
        if (feature.geometry !is com.cube.geojson.LineString) {
            fail("Steps must be linestring")
        }

        return Steps(
                convertLevel(feature),
                convertGeometry(feature.geometry) as LineString,
                feature.properties.getOrElse("incline") { "up" } == "down"
        )
    }

    /**
     * Converts footway
     */
    private fun convertFootway(feature: Feature): Footway {
        if (feature.geometry !is com.cube.geojson.LineString) {
            fail("Footway must be linestring")
        }

        return Footway(
                convertLevel(feature),
                convertGeometry(feature.geometry) as LineString
        )
    }

    /**
     * Converts elevator
     */
    private fun convertElevator(feature: Feature): Elevator {
        if (feature.geometry !is com.cube.geojson.Point) {
            fail("Elevator must be point")
        }

        return Elevator(
                convertLevel(feature),
                convertGeometry(feature.geometry) as Point
        )
    }

    /**
     * Converts beacon
     */
    private fun convertBeacon(feature: Feature): BleBeacon {
        if (!isBeacon(feature)) {
            fail("This is not beacon")
        }

        if (feature.geometry !is com.cube.geojson.Point) {
            fail("Beacon must be point")
        }

        if (feature.properties[beaconKey] != "ble") {
            fail("'ble' beacons supported only")
        }

        return BleBeacon(
                feature.properties.getOrElse(beaconMacKeyProperty, { fail(beaconMacKeyProperty + "is missing") }) as String,
                stringToDegree(feature.properties.getOrElse(orientationKeyProperty, { fail(orientationKeyProperty + "is missing") }) as String),
                convertLevel(feature),
                convertGeometry(feature.geometry) as Point
        )
    }

    /**
     * Converts room
     */
    private fun convertRoom(feature: Feature): Room {
        if (!isRoom(feature)) {
            fail("This is not room (polygon)")
        }

        val stairs = feature.properties.getOrElse("stairs") { "no" } as String == "yes"
        val escalators = feature.properties.getOrElse("conveying") { "no" } as String == "yes"
        val elevator = feature.properties.getOrElse("highway") { "" } as String == "elevator"

        val room = Room(
                convertLevel(feature),
                convertGeometry(feature.geometry)
        )

        room.levelTravelType = when {
            escalators && stairs -> LevelTravelType.ESCALATOR
            stairs -> LevelTravelType.STAIRS
            elevator -> LevelTravelType.ELEVATOR
            else -> LevelTravelType.NONE
        }

        room.roomType = when (feature.properties["indoor"]) {
            "room" -> RoomType.ROOM
            "corridor" -> RoomType.CORRIDOR
            else -> RoomType.OTHER
        }

        val female = feature.properties.getOrElse("female") {"no"} == "yes"
        val male = feature.properties.getOrElse("male") {"no"} == "yes"
        room.toiletAmenity = when (feature.properties["amenity"]) {
            "toilets" -> when {
                male && !female -> ToiletAmenity.MALE
                !male && female -> ToiletAmenity.FEMALE
                else -> ToiletAmenity.BOTH
            }
            else -> ToiletAmenity.NONE
        }

        return room
    }

    /**
     * It converts door
     */
    private fun convertDoor(feature: Feature): Door {
        if (feature.geometry !is com.cube.geojson.Point) {
            fail("Door must be point")
        }

        val door = Door(
                convertLevel(feature),
                convertGeometry(feature.geometry) as Point
        )

        door.doorType = when (feature.properties["entrance"]) {
            "yes" -> DoorType.ENTRANCE
            else -> DoorType.GENERIC
        }

        return door
    }

    /**
     * It converts unknown
     * @note It could be rejected
     */
    private fun convertUnknown(feature: Feature): Element {
        return Element(
                convertLevel(feature),
                convertGeometry(feature.geometry)
        )
    }

    /**
     * It sets common properties for all elements
     */
    private fun absortCommonProperties(element: Element, feature: Feature): Element {
        feature.properties.getOrElse("name") { null }?.let {
            element.name = it.toString()
        }

        return element
    }

    /**
     * It parse level in string ([value]) to [Levels]
     */
    private fun parseLevelFromString(value: String): Levels {
        val found = levelRegexp.matchEntire(value)
        found ?: fail("Invalid level range")
        return when (found.groups[3] == null) {
            true -> Levels(found.groups[1]!!.value.toDouble(), found.groups[1]!!.value.toDouble())
            false -> Levels(found.groups[1]!!.value.toDouble(), found.groups[3]!!.value.toDouble())
        }
    }

    /**
     * It converts level
     */
    private fun convertLevel(feature: Feature): Levels {
        return parseLevelFromString(feature.properties.getValue("level") as String)
    }

    /**
     * It converts GeoJSON geometry to JTS geometry
     */
    private fun convertGeometry(featureGeometry: GeoJsonObject): Geometry {
        return geometryConverter.convert(featureGeometry)
    }

    /**
     * Throws error
     */
    private fun fail(message: String): Nothing {
        throw IllegalArgumentException(message)
    }

    /**
     * It converts degree to [Double] from [String]
     */
    private fun stringToDegree(input: String): Double {
        return when (input.toDoubleOrNull()) {
            null -> fail("Degree is not number")
            in 0..360 -> input.toDouble()
            else -> fail("Degree is not in range 0-360")
        }
    }
}