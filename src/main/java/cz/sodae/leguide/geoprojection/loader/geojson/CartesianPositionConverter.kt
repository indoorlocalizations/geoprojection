package cz.sodae.leguide.geoprojection.loader.geojson

import com.cube.geojson.*
import cz.sodae.leguide.geoprojection.loader.FeatureCenterFinder
import cz.sodae.leguide.geoprojection.utils.GeographicTools
import cz.sodae.leguide.geoprojection.utils.WSG84Coordinates
import org.locationtech.jts.geom.Coordinate

/**
 * Class converts coordinates between WSG84 and cartesian meters system
 */
class CartesianPositionConverter(val center: WSG84Coordinates) {

    var projection: GeographicTools.Projection = GeographicTools.OwnProjection(center)

    public fun convert(position: LngLatAlt): Coordinate {
        return convert(convertCoordinates(position))
    }

    public fun convertToPoint(position: WSG84Coordinates): cz.sodae.leguide.geoprojection.Point {
        return projection.covertToPoint(position)
    }

    public fun convert(position: WSG84Coordinates): Coordinate {
        val point = projection.covertToPoint(position)
        return Coordinate(point.x, point.y, point.z)
    }

    public fun convert(position: cz.sodae.leguide.geoprojection.Point): WSG84Coordinates {
        return projection.convertToWSG84(position)
    }

    private fun convertCoordinates(it: LngLatAlt): WSG84Coordinates {
        return WSG84Coordinates(it.longitude, it.latitude, it.altitude)
    }

}
