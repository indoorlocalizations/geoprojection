package cz.sodae.leguide.geoprojection.loader

import com.cube.geojson.FeatureCollection
import com.cube.geojson.GeoJsonObject
import com.cube.geojson.LngLatAlt
import com.cube.geojson.gson.GeoJsonObjectAdapter
import com.cube.geojson.gson.LngLatAltAdapter
import com.google.gson.GsonBuilder
import cz.sodae.leguide.geoprojection.building.Building
import cz.sodae.leguide.geoprojection.loader.geojson.CartesianPositionConverter
import cz.sodae.leguide.geoprojection.loader.geojson.ElementsConverter
import cz.sodae.leguide.geoprojection.loader.geojson.GeoJSONToGeometryConverter

/**
 * Class converts [geojson] file to [building] with [elements] and provides [center] and [cartesian] converter
 */
open class Loader(val geojson: String) {

    private var identifierCounter = 0

    private val featureCollection = geojson.let {
        val builder = GsonBuilder()
        builder.registerTypeAdapter(GeoJsonObject::class.java, GeoJsonObjectAdapter())
        builder.registerTypeAdapter(LngLatAlt::class.java, LngLatAltAdapter())
        val gson = builder.create()

        gson.fromJson<FeatureCollection>(geojson, FeatureCollection::class.java)
    }

    val centerFinder = FeatureCenterFinder()

    val wsg84center = centerFinder.find(featureCollection)

    val cartesian = CartesianPositionConverter(wsg84center)

    val center = cartesian.convert(cartesian.center)

    val elements = featureCollection.let {
        val geoConverter = GeoJSONToGeometryConverter(cartesian)
        val el = ElementsConverter(geoConverter)
        el.convert(featureCollection).map {
            it.identifier = "element-" + (identifierCounter++).toString().padStart(6, '0')
            it
        }
    }

    val building = Building(elements)

}
