package cz.sodae.leguide.geoprojection.utils

import cz.sodae.leguide.geoprojection.Point
import kotlin.math.absoluteValue
import kotlin.math.pow

/**
 * Converts angle in degree to number <0, 360)
 */
fun normalizeAngle(angle: Double): Double {
    val normalized = (angle % 360)
    return when (normalized) {
        in 0..360 -> normalized
        else -> 360 + normalized
    }
}

/**
 * Calculate shortest distance between two degree
 * @example 10deg and 15deg = 5deg
 * @example 359deg and 2deg = 3deg
 */
fun angleDistance(degree: Double, second: Double): Double {
    val distance = degree - second
    return when {
        distance > 180 -> distance - 360
        distance < -180 -> distance + 360
        else -> distance
    }
}

/**
 * Calculate angle between two point on the edge of circle with radius (r)
 */
fun twoPointAngleOnCircle(point: Point, point2: Point, r: Double): Double {
    return Math.cos(
            (2*r*r - point.distance(point2).absoluteValue.pow(2)).div(
                    2*r*r
            )
    )
}

/**
 * Calculate angle between point and center
 */
fun anglePoint2D(center: Point, point: Point): Double {
    return Math.atan2(point.y - center.y, point.x - center.x) * (180 / Math.PI)
}
