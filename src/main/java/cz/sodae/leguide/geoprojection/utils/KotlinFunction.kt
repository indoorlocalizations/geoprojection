package cz.sodae.leguide.geoprojection.utils

/**
 * Combine functions
 * @author https://gist.github.com/geoand/d629f5d3911c11c695742ab61951e7f8
 */
fun <T1, T2> Collection<T1>.combine(other: Iterable<T2>): List<Pair<T1, T2>> {
    return combine(other, {thisItem: T1, otherItem: T2 -> Pair(thisItem, otherItem) })
}

fun <T1, T2, R> Collection<T1>.combine(other: Iterable<T2>, transformer: (thisItem: T1, otherItem:T2) -> R): List<R> {
    return this.flatMap { thisItem -> other.map { otherItem -> transformer(thisItem, otherItem) }}
}

/**
 * Calculates median in Double collection
 * @invariant not empty
 */
fun Collection<Double>.median(): Double {
    //if (isEmpty()) return Double.NaN
    val sorted = this.sorted()
    val mid = (sorted.size / 2)
    return when (sorted.size.rem(2)) {
        1 -> sorted[mid]
        else -> (sorted[mid] + sorted[mid - 1]) / 2
    }
}