package cz.sodae.leguide.geoprojection.utils

import cz.sodae.leguide.geoprojection.Point
import cz.sodae.leguide.geoprojection.graph2.LevelPoint

data class LevelPointWithDistance(val levelPoint: LevelPoint, val totalDistance: Double)

/**
 * Tool to split string line to points with distance [granularityDistance]. It supports multi level
 * @param granularityDistance maximum distance between points
 */
fun pathToPoints(path: List<LevelPoint>, granularityDistance: Double): List<LevelPointWithDistance> {
    if (path.size in 0..1) return path.map { LevelPointWithDistance(it, 0.0) }

    var cumulative = 0.0
    return (listOf(path[0]) + path) // it guarantees first point in result
            .zipWithNext()
            .flatMap {
                val distance = it.first.point.distance(it.second.point)

                val list = mutableListOf<LevelPointWithDistance>()

                var partDistance = granularityDistance

                while (partDistance < distance) {
                    val coef = partDistance / distance
                    list.add(LevelPointWithDistance(
                            LevelPoint(Point(
                                    (it.first.point.x * coef + it.second.point.x * (1 - coef)),
                                    (it.first.point.y * coef + it.second.point.y * (1 - coef)),
                                    0.0
                            ), if (coef >= 0.5) it.first.level else it.second.level),
                            cumulative + partDistance
                    ))
                    partDistance += granularityDistance
                }

                cumulative += distance

                list
            }
}