package cz.sodae.leguide.geoprojection.utils

/**
 * Converts angle in degree to compass degree
 * @param angle sin(0) = 0 is visually top like sin(0) = north
 * @note It does not shift angle by 90deg!
 */
fun convertToCompassAngle(angle: Double) = normalizeAngle(-angle)

/**
 * Converts compass degree to angle in degree
 * @return angle by compass input, sin(0) = 0 is visually top like sin(result = 0) = north
 * It does not shift angle by 90deg!
 */
fun convertToCircleAngle(compass: Double) = normalizeAngle(-compass)
