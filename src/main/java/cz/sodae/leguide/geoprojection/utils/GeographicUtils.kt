package cz.sodae.leguide.geoprojection.utils

import cz.sodae.leguide.geoprojection.Point
import onethreeseven.geo.projection.ProjectionTransverseMercator
import onethreeseven.geo.projection.ProjectionUTM.centralMeridianForZone

public data class WSG84Coordinates(val longitude: Double, val latitude: Double, val altitude: Double = 0.0)

/**
 * Tools to work with WGS84
 */
object GeographicTools {

    private const val EARTH_RADIUS = 6371 * 1000

    /**
     * Distance between two points in WGS84
     * If you want to ignore altitude, both altitude must be same.
     * @param second coordination
     * @return meters
     */
    fun distance(first: WSG84Coordinates, second: WSG84Coordinates): Double {
        val lat1 = first.latitude
        val lat2 = second.latitude
        val lon1 = first.longitude
        val lon2 = second.longitude
        val el1 = first.altitude
        val el2 = second.altitude

        val latDistance = Math.toRadians(lat2 - lat1)
        val lonDistance = Math.toRadians(lon2 - lon1)
        val a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + (Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2))
        val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))

        var distance = EARTH_RADIUS * c // convert to meters
        val height = el1 - el2

        distance = Math.pow(distance, 2.0) + Math.pow(height, 2.0)
        return Math.sqrt(distance)
    }

    interface Projection {

        /**
         * Convert cartesian projection to WGS84
         * @param position is second point to transformation
         */
        fun convertToWSG84(position: Point): WSG84Coordinates

        /**
         * Convert WSG84 to cartesian projection
         * @param coordinates is second point
         */
        fun covertToPoint(coordinates: WSG84Coordinates): Point
    }

    /**
     * Projection which converts WGS84 to cartesian meters like UTM
     * @param reference specify center to smaller error
     */
    class OwnProjection(reference: WSG84Coordinates) : Projection {

        val projection = ProjectionTransverseMercator(
                1.0, // enough for building
                reference.longitude,
                reference.latitude,
                1.0,
                6378137.0,
                6356752.3,
                0.0,
                0.0
        )

        override fun convertToWSG84(position: Point): WSG84Coordinates {
            return projection.cartesianToGeographic(doubleArrayOf(position.x, position.y)).let {
                WSG84Coordinates(it[1], it[0], 0.0)
            }

        }

        override fun covertToPoint(coordinates: WSG84Coordinates): Point {
            return projection.geographicToCartesian(coordinates.latitude, coordinates.longitude).let {
                Point(it[0], it[1], 0.0)
            }
        }
    }

}