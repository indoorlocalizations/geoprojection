package cz.sodae.leguide.geoprojection.utils

import java.util.Random

/**
 * Random selector based on probability
 * @todo issue: min probability is 1/[scale]
 */
class RandomSelector<T> {

    private val items = mutableListOf<ProbabilityItem>()
    private val rand = Random()
    private var probabilitySum = 0
    private val scale = 10000000

    /** Return one random element by probability */
    fun pick(): T {
        var i = 0
        var sum = 0.0

        if (probabilitySum == 0) {
            i = rand.nextInt(items.size)
        } else {
            val index = rand.nextInt(probabilitySum)

            while (sum < index) {
                sum += items[i].relativeProbability
                i++
            }
        }

        return items[Math.max(0, i - 1)].item
    }

    fun add(item: T, probability: Double) {
        val scaledProbability = (probability * scale).toInt()
        probabilitySum += scaledProbability
        items.add(ProbabilityItem(scaledProbability, item))
    }

    fun clear() {
        probabilitySum = 0
        items.clear()
    }

    private inner class ProbabilityItem(var relativeProbability: Int, var item: T)

}