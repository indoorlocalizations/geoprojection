package cz.sodae.leguide.geoprojection.graph

import cz.sodae.leguide.geoprojection.building.Building
import cz.sodae.leguide.geoprojection.elements.*
import cz.sodae.leguide.geoprojection.toXYPoint
import org.jgrapht.graph.DefaultWeightedEdge
import org.jgrapht.graph.WeightedMultigraph

/**
 * Overview graph of building
 * Graph vertex represents element by level. Graph edge represents connection between two element by level (e.g. door and two room)
 */
class OverviewGraph(private val building: Building) {

    val graph: WeightedMultigraph<DistinctLevelElement, DefaultWeightedEdge>

    init {
        val graphBuilder = WeightedMultigraph.builder<DistinctLevelElement, DefaultWeightedEdge>(DefaultWeightedEdge::class.java)
        building.elementsByLevel.toList().forEach { pair ->
            pair.second.forEach {
                graphBuilder.addVertex(DistinctLevelElement(it, pair.first))
            }
        }
        building.elements.forEach {
            when (it) {
                is Door -> it.let { door ->
                    building
                            .nearIs<Room>(it.point, it.level.lower, 0.1)
                            .forEach {
                                graphBuilder.addEdge(
                                        DistinctLevelElement(door, it.level.lower),
                                        DistinctLevelElement(it, it.level.lower)
                                )
                            }
                }
                is Footway -> it.let { footway ->
                    footway.geometry
                            .coordinates
                            .map { it.toXYPoint() }
                            .forEach { point ->
                                building
                                        .nearIs<Element>(point, it.level.lower, 3.0)
                                        .filter { it is Door || it is Footway && it != footway || it is Steps || (it is Room && it.levelTravelType == LevelTravelType.NONE) }
                                        .forEach {
                                            graphBuilder.addEdge(
                                                    DistinctLevelElement(footway, it.level.lower),
                                                    DistinctLevelElement(it, it.level.lower)
                                            )
                                        }
                            }
                }
                is Steps -> it.let { stairs ->
                    graphBuilder.addEdge(
                            DistinctLevelElement(stairs, it.level.lower),
                            DistinctLevelElement(stairs, it.level.upper)
                    )
                    building
                            .nearIs<Element>(stairs.lowerPoint, it.level.lower, 3.0)
                            .filter { it is Door || it is Footway || (it is Steps && it != stairs) || (it is Room && it.levelTravelType == LevelTravelType.NONE) }
                            .forEach {
                                graphBuilder.addEdge(
                                        DistinctLevelElement(stairs, it.level.lower),
                                        DistinctLevelElement(it, it.level.lower)
                                )
                            }
                    building
                            .nearIs<Element>(stairs.upperPoint, it.level.upper, 3.0)
                            .filter { it is Door || it is Footway || (it is Steps && it != stairs) || (it is Room && it.levelTravelType == LevelTravelType.NONE) }
                            .forEach {
                                graphBuilder.addEdge(
                                        DistinctLevelElement(stairs, it.level.upper),
                                        DistinctLevelElement(it, it.level.upper)
                                )
                            }
                }
                is Elevator -> it.let { elevator ->
                    val elevatorLevels = building.levels
                            .filter { it in elevator.level }

                    elevatorLevels
                            .zip(elevatorLevels)
                            .filter { it.first != it.second }
                            .forEach {
                                graphBuilder.addEdge(
                                        DistinctLevelElement(elevator, it.first),
                                        DistinctLevelElement(elevator, it.second)
                                )
                            }

                    elevatorLevels
                            .map { level ->
                                building
                                        .nearIs<Door>(elevator.point, level, 2.0)
                                        .forEach {
                                            graphBuilder.addEdge(
                                                    DistinctLevelElement(elevator, it.level.upper),
                                                    DistinctLevelElement(it, it.level.upper)
                                            )
                                        }
                            }
                }
            }
        }
        graph = graphBuilder.build()
    }

    /**
     * Returns elements with specific level
     */
    fun distinctElements(): List<DistinctLevelElement> {
        return graph.vertexSet().toList()
    }

    /**
     * Returns edges between distinct elements
     */
    fun neighbours(): List<Pair<DistinctLevelElement, DistinctLevelElement>> {
        return graph.edgeSet()
                .map { graph.getEdgeSource(it) to graph.getEdgeTarget(it) }
                .filter { it.first.element != it.second.element }
                .map { it.first to it.second }
    }

    /**
     * Describes element on specific level
     * @note data class because graph uses (hash) map
     */
    data class DistinctLevelElement(val element: Element, val level: Double)

}
