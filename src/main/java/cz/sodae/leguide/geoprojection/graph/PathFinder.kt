package cz.sodae.leguide.geoprojection.graph

import cz.sodae.leguide.geoprojection.Point
import cz.sodae.leguide.geoprojection.building.Building
import cz.sodae.leguide.geoprojection.elements.*
import cz.sodae.leguide.geoprojection.model.providePrecisionModel
import cz.sodae.leguide.geoprojection.toXYPoint
import cz.sodae.leguide.geoprojection.utils.combine
import org.jgrapht.alg.DijkstraShortestPath
import org.jgrapht.graph.DefaultWeightedEdge
import org.jgrapht.graph.WeightedMultigraph
import org.jgrapht.graph.builder.UndirectedWeightedGraphBuilderBase
import org.locationtech.jts.geom.Coordinate
import org.locationtech.jts.geom.Geometry
import org.locationtech.jts.geom.GeometryFactory
import org.locationtech.jts.triangulate.ConformingDelaunayTriangulationBuilder
import kotlin.math.absoluteValue

/**
 * Path finder builds paths
 * This class does not support export and import of paths
 * @deprecated use graph2.PathFinder
 */
class PathFinder(private val building: Building, overviewGraph: OverviewGraph) {
    private val factory = GeometryFactory(providePrecisionModel())

    private val highwaysVertices = mutableListOf<Vertex>()
    private val verticesInsideSmallVertexMap = mutableMapOf<OverviewGraph.DistinctLevelElement, PreparedVertices>()
    private val graph = createGraph(overviewGraph)

    /**
     * Speed is "two meters" per level + slow down (2meters) and speed up (2meters)
     */
    private fun elevatorApproxDistance(level: Double, levelTwo: Double): Double {
        return (((level - levelTwo).absoluteValue) * 2.5) + 2 * 2
    }

    private fun prepareSubgraphDoorVertices(graph: UndirectedWeightedGraphBuilderBase<Vertex, Edge, *, *>, element: Door): PreparedVertices {
        val vertex = Vertex(
                element.point,
                element.level.lower,
                element
        )
        graph.addVertex(vertex)
        return PreparedVertices(listOf(vertex), listOf(vertex))
    }

    private fun prepareSubgraphStepsVertices(graph: UndirectedWeightedGraphBuilderBase<Vertex, Edge, *, *>, element: Steps): PreparedVertices {
        val coordinates = element.geometry.coordinates
        val normalizedCoordinates = when {
            element.inclineDown -> coordinates.reversed()
            else -> coordinates.toList()
        }
        val list = mutableListOf<Vertex>()
        normalizedCoordinates.forEach {
            val vertex = Vertex(
                    it.toXYPoint(),
                    when {
                        list.size * 2 >= coordinates.size -> element.level.upper
                        else -> element.level.lower
                    },
                    element
            )
            graph.addVertex(vertex)
            list.add(vertex)
        }
        list.zipWithNext { a, b -> graph.addEdge(a, b, a.point.distance(b.point)) }

        return PreparedVertices(listOf(list.first(), list.last()), list)
    }


    private fun prepareSubgraphFootwayVertices(graph: UndirectedWeightedGraphBuilderBase<Vertex, Edge, *, *>, element: Footway): PreparedVertices {
        val coordinates = element.geometry.coordinates
        val list = mutableListOf<Vertex>()
        coordinates.forEach {
            val vertex = Vertex(
                    it.toXYPoint(),
                    element.level.lower,
                    element
            )
            graph.addVertex(vertex)
            list.add(vertex)
        }
        list.zipWithNext { a, b -> graph.addEdge(a, b, a.point.distance(b.point)) }

        return PreparedVertices(list, list)
    }

    private fun prepareSubgraphElevatorVertices(graph: UndirectedWeightedGraphBuilderBase<Vertex, Edge, *, *>, element: Elevator): PreparedVertices {
        val stops = building.levels.filter { it in element.level }.map {
            Vertex(
                    element.point,
                    it,
                    element
            )
        }
        stops.forEach {
            graph.addVertex(it)
        }
        stops.combine(stops).filter { it.first != it.second }.forEach {
            graph.addEdge(it.first, it.second, elevatorApproxDistance(it.first.level, it.second.level))
        }

        return PreparedVertices(stops, stops)
    }

    private fun prepareSubgraphRoomVertices(graph: UndirectedWeightedGraphBuilderBase<Vertex, Edge, *, *>, element: Room): PreparedVertices {
        if (element.levelTravelType != LevelTravelType.NONE) {
            return PreparedVertices(listOf(), listOf())
        }

        val builder = ConformingDelaunayTriangulationBuilder()
        builder.setSites(element.geometry)
        builder.setConstraints(element.geometry)

        var vertices = mutableListOf<Vertex>()

        builder.subdivision.visitTriangles({
            val midVertices = it
                    .map { it.toLineSegment().midPoint() }
                    .filter { element.geometry.contains(factory.createPoint(it)) }
                    .map {
                        Vertex(
                                it.toXYPoint(),
                                element.level.lower,
                                element
                        )
                    }
            vertices.addAll(midVertices)
            midVertices.combine(midVertices).filter { it.first != it.second }.forEach {
                graph.addEdge(it.first, it.second)
            }

        }, false)

        return PreparedVertices(vertices, vertices)
    }

    fun edges(level: Double): List<Pair<Point, Point>> {
        return graph
                .edgeSet()
                .filter { level == it.source.level || level == it.target.level }
                .map {
                    it.source.point to it.target.point
                }
    }

    private fun createGraph(overviewGraph: OverviewGraph): WeightedMultigraph<Vertex, Edge> {

        var start = System.currentTimeMillis()

        val graph = WeightedMultigraph.builder<Vertex, Edge>(Edge::class.java)

        overviewGraph.distinctElements().forEach {
            val prepared = when (it.element) {
                is Door -> prepareSubgraphDoorVertices(graph, it.element)
                is Steps -> prepareSubgraphStepsVertices(graph, it.element)
                is Elevator -> prepareSubgraphElevatorVertices(graph, it.element)
                is Room -> prepareSubgraphRoomVertices(graph, it.element)
                is Footway -> prepareSubgraphFootwayVertices(graph, it.element)
                else -> PreparedVertices(listOf(), listOf())
            }

            highwaysVertices.addAll(when (it.element) {
                is Elevator -> prepared.findable
                is Steps -> prepared.findable
                is Door -> prepared.findable
                is Footway -> prepared.findable
                else -> listOf()
            })

            verticesInsideSmallVertexMap[it] = prepared

        }

        overviewGraph
                .neighbours()
                .forEach {
                    verticesInsideSmallVertexMap[it.first]!!.connectible
                            .combine(verticesInsideSmallVertexMap[it.second]!!.connectible)
                            .filter { it.first.point.distance(it.second.point) <= 3.0 }
                            .filter {
                                it.first.level == it.second.level
                            }
                            .filter {
                                isVisible(building.getGeometryOnLevel(it.first.level), it.first.point, it.second.point)
                            }
                            .forEach {
                                check(it.first.level == it.second.level)
                                graph.addEdge(it.first, it.second, it.first.point.distance(it.second.point))
                            }
                }

        val t = System.currentTimeMillis() - start

        println("End: %d".format(t))

        return graph.build()
    }

    private fun isVisible(geometry: Geometry, point: Point, second: Point): Boolean {
        if (point.distance(second) == 0.0) return true

        val line = factory.createLineString(arrayOf(
                Coordinate(point.x, point.y, 0.0),
                Coordinate(second.x, second.y, 0.0)
        ))
        return try {
            line.intersection(geometry).equalsExact(line)
        } catch (e: Exception) {
            println("isVisible error:" + e.message)
            false
        }
    }

    private fun findNearest(level: Double, point: Point): Vertex? {

        val nearestHighway = {
            highwaysVertices
                    .sortedBy { it.point.distance(point) }
                    .firstOrNull { it.level == level && isVisible(building.getGeometryOnLevel(level), it.point, point) }
        }

        return building.roomAt(point, level)?.let { room ->
            val element = OverviewGraph.DistinctLevelElement(room, level)
            verticesInsideSmallVertexMap[element]?.let {
                it.findable
                        .sortedBy { it.point.distance(point) }
                        .firstOrNull {
                            it.level == level && isVisible(room.geometry, it.point, point)
                        }
            }
        } ?: nearestHighway()
    }

    fun findPath(from: Point, to: Point, levelFrom: Double, levelTo: Double): List<Vertex> {
        if (from == to && levelFrom == levelTo) {
            return listOf(createSpecificVertex(from, levelFrom))
        }

        val nearestFrom = findNearest(levelFrom, from)
        val nearestTo = findNearest(levelTo, to)

        return when {
            nearestFrom == null -> listOf()
            nearestTo == null -> listOf()

            levelFrom == levelTo && isVisible(building.getGeometryOnLevel(levelFrom), from, to) -> listOf(
                    createSpecificVertex(from, levelFrom),
                    createSpecificVertex(to, levelTo)
            )

            else -> {
                val path = DijkstraShortestPath.findPathBetween<Vertex, Edge>(
                        graph,
                        nearestFrom,
                        nearestTo
                ) ?: return listOf()

                check(nearestFrom.level == levelFrom)
                check(nearestTo.level == levelTo)

                val list = listOf(createSpecificVertex(from, levelFrom)) + path.zipWithNext().flatMap {
                    when {
                        it.first.source == it.second.source -> listOf(it.first.target, it.first.source, it.second.target)
                        it.first.source == it.second.target -> listOf(it.first.target, it.first.source, it.second.source)
                        it.first.target == it.second.source -> listOf(it.first.source, it.first.target, it.second.target)
                        it.first.target == it.second.target -> listOf(it.first.source, it.first.target, it.second.source)
                        else -> listOf()
                    }
                } + listOf(createSpecificVertex(to, levelTo))
                return optimisation(list)
            }
        }
    }

    private fun optimisation(list: List<Vertex>): List<Vertex> {
        if (list.size <= 2) {
            return list
        }

        val result = mutableListOf(list.first())

        var used = 0
        val curr = list[0]
        for (x in 0..(list.size - 3)) {
            val nextNext = list[x + 2]
            val sameLevel = nextNext.level == curr.level
            val visibleNeighbours = sameLevel && isVisible(building.getGeometryOnLevel(curr.level), curr.point, nextNext.point)

            used++
            if (!visibleNeighbours) {
                break
            }
        }

        for (x in used..(list.size - 1)) {
            result.add(list[x])
        }

        return result
    }

    private fun createSpecificVertex(point: Point, level: Double): Vertex {
        return Vertex(
                point,
                level,
                PointElement(
                        Levels(level, level),
                        factory.createPoint(Coordinate(point.x, point.y))
                )
        )
    }

    data class Vertex(val point: Point, val level: Double, val element: Element)

    class Edge : DefaultWeightedEdge() {
        public override fun getTarget(): Vertex {
            return super.getTarget() as Vertex
        }

        public override fun getSource(): Vertex {
            return super.getSource() as Vertex
        }
    }

    /**
     * Connectible vertices can be connected with other elements vertices
     * Findable can be find by nearest
     * @invariant connectible is subset of findable
     */
    private class PreparedVertices(val connectible: List<Vertex>, val findable: List<Vertex>)


}