package cz.sodae.leguide.geoprojection.graph2

import cz.sodae.leguide.geoprojection.Point

/**
 * Point with level for path finding ([PathFinder])
 */
data class LevelPoint(
        val point: Point,
        val level: Double,
        val elementId: String? = null
)
