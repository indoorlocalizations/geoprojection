package cz.sodae.leguide.geoprojection.graph2

import cz.sodae.leguide.geoprojection.Point
import cz.sodae.leguide.geoprojection.building.Building
import cz.sodae.leguide.geoprojection.model.providePrecisionModel
import org.jgrapht.alg.DijkstraShortestPath
import org.jgrapht.graph.DefaultWeightedEdge
import org.jgrapht.graph.WeightedMultigraph
import org.locationtech.jts.geom.Coordinate
import org.locationtech.jts.geom.Geometry
import org.locationtech.jts.geom.GeometryFactory

/**
 * Search path between two points
 */
class PathFinder(private val building: Building, private val structure: GraphStructure) {

    /**
     * Geometry factory
     */
    private val factory = GeometryFactory(providePrecisionModel())

    /**
     * Built graph from [structure]
     */
    private val graph = structure.let { structure ->
        val builder = WeightedMultigraph.builder<GraphPoint, DefaultWeightedEdge>(DefaultWeightedEdge::class.java)
        val mapPoints = structure.points.associateBy { it.id }
        structure.points.forEach {
            builder.addVertex(it)
        }
        structure.edges.forEach {
            builder.addEdge(mapPoints[it.graphPointIdStart], mapPoints[it.graphPointIdEnd], it.weight)
        }

        builder.build()
    }

    /**
     * @see [GraphPoint.highway]
     */
    private val highways = structure.points.filter { it.highway }

    /**
     * Group of vertices by element, it is used for search nearest point in element
     */
    private val verticesByElement = structure.points.groupBy { it.elementId }

    /**
     * Finds nearest point around [levelPoint] in element where is [levelPoint]
     */
    fun findNearest(levelPoint: LevelPoint): GraphPoint? {
        val nearestHighway = {
            highways
                    .sortedBy { toPoint(it).distance(levelPoint.point) }
                    .firstOrNull {
                        isVisible(
                                building.getGeometryOnLevel(levelPoint.level),
                                graphPointToLevelPoint(it),
                                levelPoint
                        )
                    }
        }

        return building.roomAt(levelPoint.point, levelPoint.level)?.let { room ->
            verticesByElement[room.identifier]?.let {
                it
                        .filter { it.findable }
                        .filter { it.level == levelPoint.level }
                        .sortedBy { toPoint(it).distance(levelPoint.point) }
                        .firstOrNull {
                            isVisible(room.geometry, graphPointToLevelPoint(it), levelPoint)
                        }
            }
        } ?: nearestHighway()
    }

    /**
     * Check if there are not obstacles between two points [from] and [to] in geometry (e.g. floor level [geometry])
     */
    fun isVisible(geometry: Geometry, from: LevelPoint, to: LevelPoint): Boolean {
        if (from.level != to.level) return false
        if (from.point.distance(to.point) == 0.0) return true

        val line = factory.createLineString(arrayOf(
                Coordinate(from.point.x, from.point.y, 0.0),
                Coordinate(to.point.x, to.point.y, 0.0)
        ))
        return try {
            line.intersection(geometry).equalsExact(line)
        } catch (e: Exception) {
            println("isVisible error:" + e.message)
            false
        }
    }

    /**
     * It search path between two points
     * @return path or empty path, empty means path not found
     *
     * If there are not points around [from] and [to], it return empty path
     * If [from] and [to] is visible (see [isVisible]), it returns path ([from], [to])
     * Otherwise it try find path and optimise (see [optimisation])
     */
    fun findPath(from: LevelPoint, to: LevelPoint): List<LevelPoint> {
        if (from == to) {
            return listOf(from)
        }

        val nearestFrom = findNearest(from)
        val nearestTo = findNearest(to)

        return when {
            nearestFrom == null -> listOf()
            nearestTo == null -> listOf()

            from.level == to.level && isVisible(building.getGeometryOnLevel(from.level), from, to) -> listOf(
                    from,
                    to
            )

            else -> {
                val path = DijkstraShortestPath.findPathBetween<GraphPoint, DefaultWeightedEdge>(
                        graph,
                        nearestFrom,
                        nearestTo
                ) ?: return listOf()

                check(nearestFrom.level == from.level)
                check(nearestTo.level == to.level)

                val list = listOf(from) + path.zipWithNext().flatMap {
                    val firstSource = graph.getEdgeSource(it.first)
                    val firstTarget = graph.getEdgeTarget(it.first)
                    val secondSource = graph.getEdgeSource(it.second)
                    val secondTarget = graph.getEdgeTarget(it.second)

                    when {
                        firstSource == secondSource -> listOf(firstTarget, firstSource, secondTarget)
                        firstSource == secondTarget -> listOf(firstTarget, firstSource, secondSource)
                        firstTarget == secondSource -> listOf(firstSource, firstTarget, secondTarget)
                        firstTarget == secondTarget -> listOf(firstSource, firstTarget, secondSource)
                        else -> listOf()
                    }
                }.map(::graphPointToLevelPoint) + listOf(to)
                return optimisation(list)
            }
        }
    }

    /**
     * It removes all points between first point of [list] and latest visible with the point
     */
    private fun optimisation(list: List<LevelPoint>): List<LevelPoint> {
        if (list.size <= 2) {
            return list
        }

        val result = mutableListOf(list.first())

        var used = 0
        val curr = list[0]
        for (x in 0..(list.size - 3)) {
            val nextNext = list[x + 2]
            val sameLevel = nextNext.level == curr.level
            val visibleNeighbours = sameLevel && isVisible(
                    building.getGeometryOnLevel(curr.level),
                    curr,
                    nextNext
            )

            used++
            if (!visibleNeighbours) {
                break
            }
        }

        for (x in used..(list.size - 1)) {
            result.add(list[x])
        }

        return result
    }

    /**
     * Transforms [GraphPoint] to [LevelPoint]
     */
    private fun graphPointToLevelPoint(point: GraphPoint): LevelPoint {
        return LevelPoint(
                toPoint(point),
                point.level,
                point.elementId
        )
    }

    /**
     * Returns edges, it is useful for debugging
     */
    fun edges(level: Double): List<Pair<Point, Point>> {
        return graph
                .edgeSet()
                .map { graph.getEdgeSource(it) to graph.getEdgeTarget(it) }
                .filter { level == it.first.level || level == it.second.level }
                .map {
                    toPoint(it.first) to toPoint(it.second)
                }
    }


    /**
     * Transforms [GraphPoint] to [Point]
     */
    private fun toPoint(graphPoint: GraphPoint): Point {
        return Point(graphPoint.pointX, graphPoint.pointY, 0.0)
    }

}