package cz.sodae.leguide.geoprojection.graph2

import cz.sodae.leguide.geoprojection.Point
import cz.sodae.leguide.geoprojection.building.Building
import cz.sodae.leguide.geoprojection.elements.*
import cz.sodae.leguide.geoprojection.graph.OverviewGraph
import cz.sodae.leguide.geoprojection.model.providePrecisionModel
import cz.sodae.leguide.geoprojection.toXYPoint
import cz.sodae.leguide.geoprojection.utils.combine
import org.jgrapht.graph.DefaultWeightedEdge
import org.jgrapht.graph.WeightedMultigraph
import org.jgrapht.graph.builder.UndirectedWeightedGraphBuilderBase
import org.locationtech.jts.geom.Coordinate
import org.locationtech.jts.geom.Geometry
import org.locationtech.jts.geom.GeometryFactory
import org.locationtech.jts.triangulate.ConformingDelaunayTriangulationBuilder
import kotlin.math.absoluteValue

/**
 * Method creates prepare graph to persistence (export [exportToGraphStructure])
 */
class GraphBuilder(private val building: Building, overviewGraph: OverviewGraph) {

    /**
     * Geometry factory
     */
    private val factory = GeometryFactory(providePrecisionModel())

    /**
     * Connection vertices (door, steps, elevator, footway)
     */
    private val highwaysVertices = mutableListOf<Vertex>()

    /**
     * Map of vertices in overview vertex (by vertex)
     */
    private val verticesInsideSmallVertexMap = mutableMapOf<OverviewGraph.DistinctLevelElement, PreparedVertices>()

    /**
     * Outcome graph
     */
    private val graph = createGraph(overviewGraph)

    /**
     * Speed is "two meters" per level + slow down (2meters) and speed up (2meters)
     */
    private fun elevatorApproxDistance(level: Double, levelTwo: Double): Double {
        return (((level - levelTwo).absoluteValue) * 2.5) + 2 * 2
    }

    /**
     * Creates door vertex
     * The vertex is findable and connectible
     */
    private fun prepareSubgraphDoorVertices(graph: UndirectedWeightedGraphBuilderBase<Vertex, DefaultWeightedEdge, *, *>, element: Door): PreparedVertices {
        val vertex = Vertex(
                element.point,
                element.level.lower,
                element
        )
        graph.addVertex(vertex)
        return PreparedVertices(listOf(vertex), listOf(vertex))
    }

    /**
     * Creates steps vertices
     * Vertices are findable
     * First and last vertices are connectible
     * Vertices distance is euclid distance in meters
     */
    private fun prepareSubgraphStepsVertices(graph: UndirectedWeightedGraphBuilderBase<Vertex, DefaultWeightedEdge, *, *>, element: Steps): PreparedVertices {
        val coordinates = element.geometry.coordinates
        val normalizedCoordinates = when {
            element.inclineDown -> coordinates.reversed()
            else -> coordinates.toList()
        }
        val list = mutableListOf<Vertex>()
        normalizedCoordinates.forEach {
            val vertex = Vertex(
                    it.toXYPoint(),
                    when {
                        list.size * 2 >= coordinates.size -> element.level.upper
                        else -> element.level.lower
                    },
                    element
            )
            graph.addVertex(vertex)
            list.add(vertex)
        }
        list.zipWithNext { a, b -> graph.addEdge(a, b, a.point.distance(b.point)) }

        return PreparedVertices(listOf(list.first(), list.last()), list)
    }

    /**
     * Creates footway vertices
     * Vertices are connectible and findable
     * Vertices distance is euclid distance in meters
     */
    private fun prepareSubgraphFootwayVertices(graph: UndirectedWeightedGraphBuilderBase<Vertex, DefaultWeightedEdge, *, *>, element: Footway): PreparedVertices {
        val coordinates = element.geometry.coordinates
        val list = mutableListOf<Vertex>()
        coordinates.forEach {
            val vertex = Vertex(
                    it.toXYPoint(),
                    element.level.lower,
                    element
            )
            graph.addVertex(vertex)
            list.add(vertex)
        }
        list.zipWithNext { a, b -> graph.addEdge(a, b, a.point.distance(b.point)) }

        return PreparedVertices(list, list)
    }

    /**
     * Creates elevator vertices - one vertex at each level
     * Vertices are connectible and findable
     * Vertices distance is approximated by [elevatorApproxDistance]
     */
    private fun prepareSubgraphElevatorVertices(graph: UndirectedWeightedGraphBuilderBase<Vertex, DefaultWeightedEdge, *, *>, element: Elevator): PreparedVertices {
        val stops = building.levels.filter { it in element.level }.map {
            Vertex(
                    element.point,
                    it,
                    element
            )
        }
        stops.forEach {
            graph.addVertex(it)
        }
        stops.combine(stops).filter { it.first != it.second }.forEach {
            graph.addEdge(it.first, it.second, elevatorApproxDistance(it.first.level, it.second.level))
        }

        return PreparedVertices(stops, stops)
    }

    /**
     * Creates vertices for room.
     * Vertices in room are generated with Conforming Delaunay Triangulation which separates polygon of room to triangles
     * Vertices are connectible and findable
     * Vertices distance is euclid distance in meters
     */
    private fun prepareSubgraphRoomVertices(graph: UndirectedWeightedGraphBuilderBase<Vertex, DefaultWeightedEdge, *, *>, element: Room): PreparedVertices {
        if (element.levelTravelType != LevelTravelType.NONE) {
            return PreparedVertices(listOf(), listOf())
        }

        val builder = ConformingDelaunayTriangulationBuilder()
        builder.setSites(element.geometry)
        builder.setConstraints(element.geometry)

        var vertices = mutableListOf<Vertex>()

        builder.subdivision.visitTriangles({
            val midVertices = it
                    .map { it.toLineSegment().midPoint() }
                    .filter { element.geometry.contains(factory.createPoint(it)) }
                    .map {
                        Vertex(
                                it.toXYPoint(),
                                element.level.lower,
                                element
                        )
                    }
            vertices.addAll(midVertices)
            midVertices.combine(midVertices).filter { it.first != it.second }.forEach {
                graph.addEdge(it.first, it.second)
            }

        }, false)

        return PreparedVertices(vertices, vertices)
    }

    /**
     * Creates graph (vertices and edges)
     */
    private fun createGraph(overviewGraph: OverviewGraph): WeightedMultigraph<Vertex, DefaultWeightedEdge> {

        var start = System.currentTimeMillis()

        val graph = WeightedMultigraph.builder<Vertex, DefaultWeightedEdge>(DefaultWeightedEdge::class.java)

        overviewGraph.distinctElements().forEach {
            val prepared = when (it.element) {
                is Door -> prepareSubgraphDoorVertices(graph, it.element)
                is Steps -> prepareSubgraphStepsVertices(graph, it.element)
                is Elevator -> prepareSubgraphElevatorVertices(graph, it.element)
                is Room -> prepareSubgraphRoomVertices(graph, it.element)
                is Footway -> prepareSubgraphFootwayVertices(graph, it.element)
                else -> PreparedVertices(listOf(), listOf())
            }

            highwaysVertices.addAll(when (it.element) {
                is Elevator -> prepared.findable
                is Steps -> prepared.findable
                is Door -> prepared.findable
                is Footway -> prepared.findable
                else -> listOf()
            })

            verticesInsideSmallVertexMap[it] = prepared

        }

        overviewGraph
                .neighbours()
                .forEach {
                    verticesInsideSmallVertexMap[it.first]!!.connectible
                            .combine(verticesInsideSmallVertexMap[it.second]!!.connectible)
                            .filter {
                                it.first.level == it.second.level
                            }
                            .sortedBy { it.first.point.distance(it.second.point) }
                            .take(3)
                            .filter {
                                isVisible(building.getGeometryOnLevel(it.first.level), it.first.point, it.second.point)
                            }
                            .forEach {
                                check(it.first.level == it.second.level)
                                graph.addEdge(it.first, it.second, it.first.point.distance(it.second.point))
                            }
                }

        val t = System.currentTimeMillis() - start

        println("End: %d".format(t))

        return graph.build()
    }

    /**
     * Exports graph to serializable structure
     * This structure accepts graph2.PathFinder
     */
    fun exportToGraphStructure(): GraphStructure {
        val vertexWithIndex = graph.vertexSet().withIndex().map {
            it.value to it.index.toLong()
        }
        val vertexWithIndexMap = vertexWithIndex.toMap()
        val vertex = vertexWithIndex.map {
            GraphPoint(
                    it.second,
                    it.first.element.identifier!!,
                    it.first.point.x,
                    it.first.point.y,
                    it.first.level,
                    verticesInsideSmallVertexMap[OverviewGraph.DistinctLevelElement(it.first.element, it.first.level)]!!.findable.contains(it.first),
                    it.first in highwaysVertices,
                    it.first.element !is Steps
            )
        }
        val edges = graph.edgeSet().map {
            GraphEdge(
                    vertexWithIndexMap[graph.getEdgeSource(it)]!!,
                    vertexWithIndexMap[graph.getEdgeTarget(it)]!!,
                    graph.getEdgeWeight(it)
            )
        }
        return GraphStructure(
                vertex.toTypedArray(),
                edges.toTypedArray()
        )
    }

    /**
     * Check if there are not obstacles between two points [point] and [second] in geometry (e.g. floor level [geometry])
     */
    private fun isVisible(geometry: Geometry, point: Point, second: Point): Boolean {
        if (point.distance(second) == 0.0) return true

        val line = factory.createLineString(arrayOf(
                Coordinate(point.x, point.y, 0.0),
                Coordinate(second.x, second.y, 0.0)
        ))
        return try {
            line.intersection(geometry).equalsExact(line)
        } catch (e: Exception) {
            println("isVisible error:" + e.message)
            false
        }
    }

    /**
     * Vertex represents specific 
     */
    data class Vertex(val point: Point, val level: Double, val element: Element)

    /**
     * Connectible vertices can be connected with other elements vertices
     * Findable can be find by nearest
     * @invariant connectible is subset of findable
     */
    private class PreparedVertices(val connectible: List<Vertex>, val findable: List<Vertex>)

}