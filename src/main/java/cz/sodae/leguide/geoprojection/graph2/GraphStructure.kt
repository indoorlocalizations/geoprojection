package cz.sodae.leguide.geoprojection.graph2

import java.io.Serializable

/**
 * Serializable graph vertex
 * @param id is unique id of vertex
 * @param elementId is [cz.sodae.leguide.geoprojection.elements.Element.identifier]
 * @param pointX is X position
 * @param pointY is Y position
 * @param level is level
 * @param findable can be point for start/end of a found path
 * @param highway represents connection point (steps, door, elevator, footway)
 * @param forDisabled the vertex is able for disabled people as well (steps contains false)
 */
data class GraphPoint(
        val id: Long,
        val elementId: String,
        val pointX: Double,
        val pointY: Double,
        val level: Double,
        val findable: Boolean,
        val highway: Boolean,
        val forDisabled: Boolean
): Serializable {
    companion object {
        private const val serialVersionUID = 475918891428093043L
    }
}

/**
 * Represents edge between [GraphPoint]
 * @param weight is edge weight
 */
data class GraphEdge(
        val graphPointIdStart: Long,
        val graphPointIdEnd: Long,
        val weight: Double
): Serializable {
    companion object {
        private const val serialVersionUID = 475918891428093042L
    }
}

/**
 * Structure to save points
 */
data class GraphStructure(val points: Array<GraphPoint>, val edges: Array<GraphEdge>): Serializable {
    companion object {
        private const val serialVersionUID = 475918891428093041L
    }
}
