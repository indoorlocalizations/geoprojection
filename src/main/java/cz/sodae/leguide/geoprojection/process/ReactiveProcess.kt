package cz.sodae.leguide.geoprojection.process

import cz.sodae.leguide.geoprojection.Point
import cz.sodae.leguide.geoprojection.building.Building
import cz.sodae.leguide.geoprojection.elements.BleBeacon
import cz.sodae.leguide.geoprojection.graph2.PathFinder
import cz.sodae.leguide.geoprojection.model.*
import cz.sodae.leguide.geoprojection.model.signal.Sample
import cz.sodae.leguide.geoprojection.model.signal.filters.AverageFilterBuilder
import cz.sodae.leguide.geoprojection.model.signal.filters.KalmanFilterBuilder
import cz.sodae.leguide.geoprojection.model.signal.filters.MedianFilterBuilder
import cz.sodae.leguide.geoprojection.model.signal.filters.MedianKalmanFilterBuilder
import cz.sodae.leguide.geoprojection.utils.convertToCompassAngle
import io.reactivex.Flowable
import io.reactivex.rxkotlin.withLatestFrom
import java.util.concurrent.TimeUnit

/**
 * Debugging information (PF = particle filter) - enables with [ReactiveProcess.debugging]
 * travel paths enables with false in [ReactiveProcess.disableWallJump]
 */
class Debugging(
        val particles: List<Point>,
        val beaconsRange: List<BeaconMeasurement>,
        val beaconsPFPoint: Point,
        val beaconsPFAccuracy: Double,
        var travelEdges: List<Pair<Point, Point>>? = null,
        var travelPath: List<Point>? = null
)

/**
 * Result position
 */
class CurrentPosition(
        val time: Long, // ms
        val point: Point,
        val level: Double,
        val accuracy: Double, // m
        val movement: Boolean,
        val compass: Double,
        val debugging: Debugging?
)

/**
 * Measured signal
 */
data class Signal(
        val mac: String,
        val rssi: Double
)

typealias BeaconsDistances = Map<BleBeacon, Double>

/**
 * Processing reactive sources to filtering and localisation to output flowable [positionReactive]
 *
 * @param walkingReactive is user walking
 * @param orientationReactive user orientation in circle angles (not compass)
 * @param beaconsReactive measured RSSI and beacon MAC
 * @param building map representation
 * @param pathFinder is used only for [ParticleFilterMovementProbability] - nice to have feature
 */
class ReactiveProcess(
        val walkingReactive: Flowable<Boolean>,
        val orientationReactive: Flowable<Double>,
        val beaconsReactive: Flowable<Signal>,
        val building: Building,
        val pathFinder: PathFinder
) {

    /**
     * How often it produces new position into [positionReactive]
     */
    private val samplingTime = 100L

    /**
     * Disables [ParticleFilterMovementProbability]
     */
    private val disableWallJump = true

    /**
     * Reactive level detection
     */
    private val levelDetection: LevelDetection = LevelDetection(building.levels)

    /**
     * Particle filter model of localisation
     */
    private val beaconsParticleFilterModel = ParticleFilterBuildingProbability(building)

    /**
     * Particle filter for localisation
     */
    private val beaconsParticleFilter = ParticleFilter(beaconsParticleFilterModel, 100)

    /**
     * @see ParticleFilterMovementProbability description
     */
    private val movementParticleFilterModel = ParticleFilterMovementProbability(
            building.getGeometryOnLevel(building.levels.first()),
            pathFinder
    )
    private val movementParticleFilter = ParticleFilter(movementParticleFilterModel, 1)

    /**
     * Enable debugging object inside [positionReactive]
     */
    var debugging = false

    /**
     * Output flowable
     */
    val positionReactive = reactiveBeaconsParticleFilter().let {
        when {
            disableWallJump -> it
            else -> reactiveWallReaction(it)
        }
    }

    /**
     * Creates beacons distance filter
     * @see signal.filters see classes in this subpackage
     */
    private fun reactiveBeaconsDistance(): Flowable<BeaconsDistances> {
        val source = beaconsReactive
                .filter { building.beaconsByMac.containsKey(it.mac) }
                .withLatestFrom(orientationReactive)
                .map {
                    Sample(
                            building.beaconsByMac[it.first.mac]!!,
                            it.first.rssi,
                            it.second
                    )
                }

        //return MedianFilterBuilder(source, samplingTime).build()
        //return AverageFilterBuilder(source, samplingTime).build()
        //return KalmanFilterBuilder(source).build()
        return MedianKalmanFilterBuilder(source, samplingTime).build()
    }

    /**
     * Creates iteration transformation of particle filter for localisation
     * @see ParticleFilterBuildingProbability more details about localisation
     */
    private fun reactiveBeaconsParticleFilter(): Flowable<CurrentPosition> {
        val beaconsReactive = reactiveBeaconsDistance().share()
        return Flowable.interval(samplingTime, TimeUnit.MILLISECONDS).withLatestFrom(
                beaconsReactive,
                orientationReactive,
                walkingReactive,
                reactiveLevelDetection(beaconsReactive)
        ) { _, beacons, orientation, movement, level ->
            beaconsParticleFilterModel.changeFloor(level)
            beaconsParticleFilterModel.distances = beacons
                    .filter { it.key.level.exactlyLevel(level) }
                    .map {
                        BeaconMeasurement(it.key.point, it.value)
                    }
            beaconsParticleFilterModel.movementDetected = movement
            beaconsParticleFilterModel.orientationDegree = orientation

            beaconsParticleFilter.step()

            val position = beaconsParticleFilter.mean
            val accuracy = beaconsParticleFilter.distanceStandardDeviation

            CurrentPosition(
                    System.currentTimeMillis(),
                    position,
                    level,
                    accuracy,
                    movement,
                    convertToCompassAngle(orientation),
                    when {
                        debugging -> Debugging(
                                beaconsParticleFilter.particlePositions,
                                beaconsParticleFilterModel.distances,
                                position,
                                accuracy
                        )
                        else -> null
                    }
            )
        }
    }

    /**
     * Builds level detection
     */
    private fun reactiveLevelDetection(bufferedSignals: Flowable<Map<BleBeacon, Double>>): Flowable<Double> {
        return bufferedSignals
                .filter {
                    it.isNotEmpty()
                }
                .map {
                    it.map {
                        LevelDetection.LevelDetectionSample(it.key, it.value)
                    }
                }
                .map {
                    levelDetection.detect(it)
                }
                .distinctUntilChanged()
    }

    /**
     * Builds moving by path finder
     */
    private fun reactiveWallReaction(position: Flowable<CurrentPosition>): Flowable<CurrentPosition> {
        return position.map {
            movementParticleFilterModel.changeFloor(building.getGeometryOnLevel(it.level))
            movementParticleFilterModel.movementDetected = it.movement
            movementParticleFilterModel.orientationDegree = it.compass
            movementParticleFilterModel.setPosition(it.point, it.level)

            movementParticleFilter.step()

            CurrentPosition(
                    System.currentTimeMillis(),
                    movementParticleFilter.mean,
                    it.level,
                    it.accuracy,
                    it.movement,
                    it.compass,
                    it.debugging?.apply {
                        this.travelPath = movementParticleFilterModel.debugPath
                        this.travelEdges = pathFinder.edges(it.level)
                    }
            )
        }
    }

}
