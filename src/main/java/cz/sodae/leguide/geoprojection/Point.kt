package cz.sodae.leguide.geoprojection

import org.locationtech.jts.geom.Coordinate

/**
 * Class represents point
 */
data class Point(val x: Double, val y: Double, val z: Double): java.io.Serializable {

    /**
     * @return euclid's distance between the point and that [point]
     */
    fun distance(point: Point): Double {
        if (point == this) return 0.0
        return Math.sqrt(Math.pow(x - point.x, 2.0) + Math.pow(y - point.y, 2.0) + Math.pow(z * point.z, 2.0))
    }

    companion object {
        private const val serialVersionUID = 475918891428093049L
    }

}

/**
 * Extends JTS coordinates to conversion to [Point]
 */
fun Coordinate.toXYPoint() = Point(this.x, this.y, 0.0)

/**
 * It finds mean of [points]
 */
fun meanPoints(points: List<Point>): Point {
    check(points.isNotEmpty())
    return Point(
            points.sumByDouble { it.x } / points.size.toDouble(),
            points.sumByDouble { it.y } / points.size.toDouble(),
            points.sumByDouble { it.z } / points.size.toDouble()
    )
}
